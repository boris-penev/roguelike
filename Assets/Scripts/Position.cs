using System;

public class Position : IEquatable<Position>
{
    public int x, y;

    public Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public bool Equals(Position other)
    {
        return x == other.x && y == other.y;
    }
}
