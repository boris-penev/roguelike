using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour
{
    [System.Serializable]
    public class Count
    {
        public int minimum, maximum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns, rows;
    public Count wallCount;
    public GameObject exit, tutorialTextPrefab;
    public GameObject[] floorTiles, wallTiles, outerWallTiles;
    public GameObject[] foodTiles, enemyTiles;
    public int[] foodWeights, enemyWeights;
    internal Tile[,] boardObjects;

    private Transform boardHolder;
    private List<Position> gridPositions = new List<Position>();

    /// <summary>
    /// Return a random object from a discrete weighted distribution
    /// </summary>
    /// <typeparam name="T">The type of the objects</typeparam>
    /// <param name="objects">The objects from which to choose</param>
    /// <param name="weights">The weights of the objects</param>
    /// <param name="weightsSum">The sum of the weights</param>
    /// <returns>A random object from a discrete weighted distribution
    /// </returns>
    public T WeightedRandom<T>(T[] objects, int[] weights, int weightsSum)
    {
        int roll = Random.Range(0, weightsSum);
        T selected = objects[objects.Length - 1];
        for (int weightIndex = 0; weightIndex < weights.Length; ++weightIndex)
        {
            int weight = weights[weightIndex];
            if (roll < weight)
            {
                selected = objects[weightIndex];
                break;
            }
            roll -= weight;
        }
        return selected;
    }

    // Use this for initialization
    void Start()
    {
		
    }
	
    void InitialiseList()
    {
        gridPositions.Clear();
        for (int x = 0; x < columns; ++x)
        {
            for (int y = 0; y < rows; ++y)
            {
                gridPositions.Add(new Position(x, y));
            }
        }
    }

    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;
        boardObjects = new Tile[rows, columns];

        for (int x = -1; x < columns + 1; ++x)
        {
            for (int y = -1; y < columns + 1; ++y)
            {
                GameObject toInstantiate;
                if (x == -1 || x == columns || y == -1 || y == rows)
                {
                    toInstantiate = outerWallTiles[Random.Range(0,
                        outerWallTiles.Length)];
                }
                else
                {
                    toInstantiate = floorTiles[Random.Range(0,
                    floorTiles.Length)];
                    boardObjects[x, y] = new Tile { food = null,
                        occupant = null };
                }
                GameObject instance = Instantiate(toInstantiate,
                    new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    Position RandomPosition()
    {
        if (gridPositions.Count == 0)
        {
            return new Position(-1, -1);
        }
        int randomIndex = Random.Range(0, gridPositions.Count);
        Position randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);
        return randomPosition;
    }

    void LayoutObjectAtUniformRandom(GameObject[] tileArray, int minimum,
        int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);
        for (int i = 0; i < objectCount; ++i)
        {
            Position randomPosition = RandomPosition();
            if (randomPosition.x == -1.0f)
            {
                return;
            }
            Vector3 positionVector = new Vector3(randomPosition.x,
                randomPosition.y, 0f);
            GameObject tileChoice = tileArray[Random.Range(0,
                tileArray.Length)];
            Instantiate(tileChoice, positionVector, Quaternion.identity);
        }
    }

    void LayoutObjectAtWeightedRandom(GameObject[] tileArray, int[] weights,
        int minimum, int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);
        int weightsSum = 0;
        foreach (int weight in weights)
        {
            weightsSum += weight;
        }
        for (int i = 0; i < objectCount; ++i)
        {
            Position randomPosition = RandomPosition();
            if (randomPosition.x == -1.0f)
            {
                return;
            }
            Vector3 positionVector = new Vector3(randomPosition.x,
                randomPosition.y, 0f);
            GameObject tileChoice = WeightedRandom(tileArray, weights,
                weightsSum);
            Instantiate(tileChoice, positionVector, Quaternion.identity);
        }
    }

    void LayoutWalls()
    {
        int objectCount = Random.Range(wallCount.minimum,
            wallCount.maximum + 1);
        for (int i = 0; i < objectCount; ++i)
        {
            Position randomPosition = RandomPosition();
            if (randomPosition.x == -1.0f)
            {
                return;
            }
            Vector3 positionVector = new Vector3(randomPosition.x,
                randomPosition.y, 0f);
            GameObject tileChoice = wallTiles[Random.Range(0,
                wallTiles.Length)];
            GameObject instance = Instantiate(tileChoice, positionVector,
                Quaternion.identity) as GameObject;
            boardObjects[randomPosition.x, randomPosition.y].occupant
                = instance.GetComponent<InnerWall>() as BlockingObject;
            boardObjects[randomPosition.x, randomPosition.y].occupant
                .position = randomPosition;
        }
    }

    void LayoutFood(int level)
    {
        Count foodCount = new Count((int)Mathf.Log(level + 3, 2f) - 1,
            (int)Mathf.Log(level + 3, 2f) + 2);
        int objectCount = Random.Range(foodCount.minimum,
            foodCount.maximum + 1);
        int weightsSum = 0;
        foreach (int weight in foodWeights)
        {
            weightsSum += weight;
        }
        for (int i = 0; i < objectCount; ++i)
        {
            Position randomPosition = RandomPosition();
            if (randomPosition.x == -1.0f)
            {
                return;
            }
            Vector3 positionVector = new Vector3(randomPosition.x,
                randomPosition.y, 0f);
            GameObject tileChoice = WeightedRandom(foodTiles, foodWeights,
                weightsSum);
            GameObject instance = Instantiate(tileChoice, positionVector,
                Quaternion.identity) as GameObject;
            boardObjects[randomPosition.x, randomPosition.y].food =
                instance.GetComponent<Food>();
        }
    }

    void LayoutEnemies(int level)
    {
        Count enemyCount = new Count(0,
            (int)(Mathf.Log(level, 2f) + 0.09f * level));
        int objectCount = Random.Range(enemyCount.minimum,
            enemyCount.maximum + 1);
        int weightsSum = 0;
        foreach (int weight in enemyWeights)
        {
            weightsSum += weight;
        }
        for (int i = 0; i < objectCount; ++i)
        {
            Position randomPosition = RandomPosition();
            if (randomPosition.x == -1.0f)
            {
                return;
            }
            Vector3 positionVector = new Vector3(randomPosition.x,
                randomPosition.y, 0f);
            GameObject tileChoice = WeightedRandom(enemyTiles, enemyWeights,
                weightsSum);
            GameObject instance = Instantiate(tileChoice, positionVector,
                Quaternion.identity) as GameObject;
            boardObjects[randomPosition.x, randomPosition.y].occupant
                = instance.GetComponent<Enemy>() as BlockingObject;
            boardObjects[randomPosition.x, randomPosition.y].occupant
                .position = randomPosition;
        }
    }

    public void SetupScene(int level)
    {
        BoardSetup();
        InitialiseList();
        GameObject player = GameObject.FindWithTag("Player");
        player.transform.position = new Vector3(0f, 0f, 0f);
        boardObjects[0, 0].occupant = player.GetComponent<Player>() as
            BlockingObject;
        boardObjects[0, 0].occupant.position = new Position(0, 0);
        gridPositions.RemoveAt(0);
        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0f),
            Quaternion.identity);
        boardObjects[columns - 1, rows - 1].exit = true;
        gridPositions.RemoveAt(gridPositions.Count - 1);
        LayoutFood(level);
        LayoutEnemies(level);
        LayoutWalls();
        GameObject canvas = GameObject.Find("Canvas");
        GameManager.instance.tutorialText =
            Instantiate(tutorialTextPrefab).GetComponent<Text>();
        GameManager.instance.tutorialText.transform.SetParent(canvas.transform,
            false);
        GameManager.instance.tutorialText.enabled = false;
    }
}
