﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int food;
    public enum Type { Food, Drink };
    public Type type;
}
