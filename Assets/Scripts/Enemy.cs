using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject
{
    public int healthPoints, playerDamage, wallDamage;
    public AudioClip attackSound1, attackSound2;
    private bool skipMove = true;
    private Animator animator;
    private Player player;

    public override bool isPlayer()
    {
        return false;
    }

    public override bool isEnemy()
    {
        return true;
    }

    public void LoseHealth(int loss)
    {
        healthPoints -= loss;
        if (healthPoints <= 0)
        {
            GameManager.instance.RemoveEnemyFromList(this);
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    protected override void Start()
    {
        GameManager.instance.AddEnemyToList(this);
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").
            GetComponent<Player>();
        base.Start();
    }

    private void PerformTurn(int xDir, int yDir)
    {
        // Debug.Log("PerformTurn " + xDir + " " + yDir);
        int x = position.x + xDir;
        int y = position.y + yDir;
        if ((xDir != 0 || yDir != 0)
            && x >= 0
            && x < GameManager.instance.boardScript.columns
            && y >= 0
            && y < GameManager.instance.boardScript.rows)
        {
            AttemptMove(x, y);
            skipMove = true;
        }
    }

    protected override void AttemptMove(int x, int y)
    {
        // do not modify tile, it is passed by value
        Tile tile = GameManager.instance.boardObjects[x, y];
        // do not modify occupant, it is passed by value
        BlockingObject occupant = tile.occupant;
        if (occupant != null)
        {
            if (occupant.isPlayer())
            {
                Player hitPlayer = occupant as Player;
                animator.SetTrigger("enemyAttack");
                SoundManager.instance.RandomizeSoundEffects(attackSound1,
                    attackSound2);
                hitPlayer.LoseFood(playerDamage);
            }
            else if (occupant.isWall())
            {
                Wall hitWall = occupant as Wall;
                hitWall.DamageWall(wallDamage);
                animator.SetTrigger("enemyAttack");
            }
        }
        else
        {
            GameManager.instance.boardObjects[position.x, position.y].occupant
                = null;
            GameManager.instance.boardObjects[x, y].occupant = this;
            position.x = x;
            position.y = y;
            Move(x, y);
        }
    }

    public bool MoveEnemy()
    {
        if (skipMove)
        {
            skipMove = false;
            return false;
        }
        int xDir = 0, yDir = 0;
        var path = AStar(position, player.position);
        if (path.Count > 1)
        {
            var target = path[path.Count - 2];
            xDir = target.x - position.x;
            if (xDir == 0)
            {
                yDir = target.y - position.y;
            }
            PerformTurn(xDir, yDir);
            return true;
        }
        int xDistance = Math.Abs(player.position.x - position.x);
        int yDistance = Math.Abs(player.position.y - position.y);
        if (xDistance > 0 && xDistance >= yDistance)
        {
            xDir = player.position.x > position.x ? 1 : -1;
            yDir = 0;
            if (CanMove(xDir, yDir))
            {
                PerformTurn(xDir, yDir);
                return true;
            }
        }
        if (yDistance > 0)
        {
            xDir = 0;
            yDir = player.position.y > position.y ? 1 : -1;
            if (CanMove(xDir, yDir))
            {
                PerformTurn(xDir, yDir);
                return true;
            }
        }
        if (xDistance > 0)
        {
            xDir = player.position.x > position.x ? 1 : -1;
            yDir = 0;
            if (CanMove(xDir, yDir))
            {
                PerformTurn(xDir, yDir);
                return false;
            }
        }
        if (xDistance > 0)
        {
            xDir = player.position.x > position.x ? 1 : -1;
            yDir = 0;
            PerformTurn(xDir, yDir);
            return false;
        }
        if (yDistance > 0)
        {
            xDir = 0;
            yDir = player.position.y > position.y ? 1 : -1;
            PerformTurn(xDir, yDir);
            return false;
        }
        Debug.Log("An enemy character overlaps with the player character.");
        PerformTurn(0, 0);
        return false;
    }

    private bool CanMove(int xDir, int yDir)
    {
        Tile tile = GameManager.instance.boardObjects[position.x + xDir,
            position.y + yDir];
        BlockingObject occupant = tile.occupant;
        return occupant == null;
    }

    private List<Position> AStar(Position start, Position goal)
    {
        int rows = GameManager.instance.boardScript.rows;
        int columns = GameManager.instance.boardScript.columns;

        if (start.x == goal.x && start.y == goal.y)
        {
            return new List<Position>();
        }

        Position[,] nodes = new Position[rows, columns];

        // The set of nodes already evaluated
        var closedSet = new HashSet<Position>();
        // The set of tentative nodes to be evaluated,
        // initially containing the start node
        var openSet = new HashSet<Position>();
        // The map of navigated nodes
        Position[,] cameFrom = new Position[rows, columns];

        // Cost from start along best known path.
        double[,] gScore = new double[rows, columns];
        // Estimated total cost from start to goal through node
        double[,] fScore = new double[rows, columns];
        for (int i = 0; i < rows; ++i)
        {
            for (int j = 0; j < columns; ++j)
            {
                gScore[i, j] = double.PositiveInfinity;
                fScore[i, j] = double.PositiveInfinity;
                nodes[i, j] = new Position(i, j);
                cameFrom[i, j] = null;
            }
        }
        // get the right references
        start = nodes[start.x, start.y];
        goal = nodes[goal.x, goal.y];
        openSet.Add(start);
        gScore[start.x, start.y] = 0;
        fScore[start.x, start.y] = gScore[start.x, start.y]
                + HeuristicCostEstimate(start, goal);
        Position current;
        while (openSet.Count > 0)
        {
            current = MinFScore(openSet, fScore);
            if (current == goal)
            {
                return ReconstructPath(cameFrom, current);
            }
            openSet.Remove(current);
            closedSet.Add(current);
            List<Position> neighbours = GenerateNeighbours(nodes, current);
            foreach (var neighbour in neighbours)
            {
                if (closedSet.Contains(neighbour))
                {
                    // Ignore the neighbours that are already evaluated.
                    continue;
                }
                // Discover a new node
                if (!openSet.Contains(neighbour))
                {
                    openSet.Add(neighbour);
                }
                var tentativeGScore = gScore[current.x, current.y]
                    + DistanceBetween(current, neighbour);
                if (tentativeGScore >= gScore[neighbour.x, neighbour.y])
                {
                    // This is not a better path.
                    continue;
                }

                // This path is the best until now. Record it!
                cameFrom[neighbour.x, neighbour.y] = current;
                gScore[neighbour.x, neighbour.y] = tentativeGScore;
                fScore[neighbour.x, neighbour.y] = tentativeGScore
                    + HeuristicCostEstimate(neighbour, goal);
            }
        }
        current = MinFScore(closedSet, fScore);
        return ReconstructPath(cameFrom, current);
    }

    private double DistanceBetween(Position current, Position neighbour)
    {
        var map = GameManager.instance.boardObjects;
        int x = neighbour.x;
        int y = neighbour.y;
        if (map[x, y].occupant == null || map[x, y].occupant.isPlayer())
        {
            return 1.0;
        }
        if (map[x, y].occupant.isWall())
        {
            InnerWall wall = map[x, y].occupant as InnerWall;
            return 1.0 + wall.hitPoints;
        }
        return double.PositiveInfinity;
    }

    private List<Position> GenerateNeighbours(Position[,] nodes,
        Position current)
    {
        int x = current.x;
        int y = current.y;
        var map = GameManager.instance.boardObjects;
        int rows = GameManager.instance.boardScript.rows;
        int columns = GameManager.instance.boardScript.columns;
        var neighbours = new List<Position>();
        // generating a new position so we do not reference nodes array index
        // out of bounds
        Position[] possibleNeighbours = {
            new Position(x - 1, y), new Position(x + 1, y),
            new Position(x, y - 1), new Position(x, y + 1)};
        foreach (var neighbour in possibleNeighbours)
        {
            int nx = neighbour.x;
            int ny = neighbour.y;
            if (0 <= nx && nx < rows && 0 <= ny && ny < columns
                && (map[nx, ny].occupant == null
                    || map[nx, ny].occupant.isPlayer()
                    || map[nx, ny].occupant.isWall()))
            {
                // adding the nodes position instead of a new one so the hash
                // set can perform a reference check
                neighbours.Add(nodes[nx, ny]);
            }
        }
        return neighbours;
    }

    private List<Position> ReconstructPath(Position[,] cameFrom,
        Position current)
    {
        var path = new List<Position>
        {
            current
        };
        while (cameFrom[current.x, current.y] != null)
        {
            current = cameFrom[current.x, current.y];
            path.Add(current);
        }
        return path;
    }

    private double HeuristicCostEstimate(Position start, Position goal)
    {
        return goal.x - start.x + goal.y - start.y;
    }

    private Position MinFScore(HashSet<Position> openSet, double[,] fScore)
    {
        double minFScore = double.PositiveInfinity;
        var minNode = new Position(-1, -1);
        foreach (var node in openSet)
        {
            if (minNode.x == -1 || fScore[node.x, node.y] < minFScore)
            {
                minFScore = fScore[node.x, node.y];
                minNode = node;
            }
        }
        return minNode;
    }
}
