using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BlockingObject : MonoBehaviour
{
    public Position position;
    public abstract bool isPlayer();
    public abstract bool isEnemy();
    public abstract bool isWall();
}
