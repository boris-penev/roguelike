﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerWall : Wall
{
    public int hitPoints;
    public Sprite damageSprite;
    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public override void DamageWall(int loss)
    {
        base.DamageWall(loss);
        spriteRenderer.sprite = damageSprite;
        hitPoints -= loss;
        if (hitPoints <= 0)
        {
            gameObject.SetActive(false);
            GameManager.instance.boardObjects[position.x, position.y].occupant
                = null;
        }
    }
}
