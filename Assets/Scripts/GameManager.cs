using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public float levelStartDelay, turnDelay;
    public int playerFoodPoints;
    internal bool levelOver, playersTurn;
    internal Text tutorialText;
    internal Tile[,] boardObjects;
    internal BoardManager boardScript;

    private bool doingSetup, enemiesMoving;
    private int level = 0, tutorialStep = 0;
    private GameObject levelImage;
    private List<Enemy> enemies;
    private Text levelText;

    public void GameOver()
    {
        enabled = false;
        levelText.text = "After " + level + " "
            + (level == 1 ? "day" : "days") + "\nyou starved.";
        levelImage.SetActive(true);
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void RemoveEnemyFromList(Enemy enemy)
    {
        enemies.Remove(enemy);
    }

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        enemies = new List<Enemy>();
        boardScript = GetComponent<BoardManager>();
    }

    void InitGame()
    {
        doingSetup = true;
        levelOver = false;
        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);
        enemies.Clear();
        boardScript.SetupScene(level);
        boardObjects = boardScript.boardObjects;
    }

    // Update is called once per frame
    void Update()
    {
        if (playersTurn || enemiesMoving || doingSetup || levelOver)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        Tutorial();
        yield return new WaitForSeconds(turnDelay);
        float longestMoveTime = turnDelay;
        for (int i = 0; i < enemies.Count; ++i)
        {
            if (enemies[i].MoveEnemy())
            {
                longestMoveTime = Math.Max(longestMoveTime, enemies[i].moveTime);
            }
        }
        yield return new WaitForSeconds(longestMoveTime);
        playersTurn = true;
        enemiesMoving = false;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        ++level;
        InitGame();
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        playersTurn = true;
        doingSetup = false;
        if (tutorialStep < 4)
        {
            Tutorial();
        }
    }

    private void Tutorial()
    {
        if (tutorialStep == 0)
        {
            tutorialText.text = "Press the arrow keys " +
                "\nto move and chop walls.";
            tutorialText.enabled = true;
            ++tutorialStep;
        }
        else if (tutorialStep == 1)
        {
            tutorialText.text = "Move on top of food\nto collect it.";
            ++tutorialStep;
        }
        else if (tutorialStep == 2)
        {
            tutorialText.text = "Reach the exit to\ncomplete the level.";
            ++tutorialStep;
        }
        else if (tutorialStep == 3)
        {
            tutorialText.enabled = false;
            if (enemies.Count > 0)
            {
                tutorialText.text = "Press the arrow keys\nto attack enemies.";
                tutorialText.enabled = true;
                ++tutorialStep;
            }
        }
        else if (tutorialStep == 4)
        {
            tutorialText.enabled = false;
        }
    }
}
