using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject
{
    public int wallDamage, enemyDamage;
    public float restartLevelDelay;
    public AudioClip moveSound1, moveSound2, eatSound1, eatSound2,
        drinkSound1, drinkSound2, gameOverSound, attackSound1, attackSound2;
    public Text foodText;

    private int food;
    private Animator animator;
#if UNITY_ANDROID
    private Vector2 touchOrigin;
#endif

    public override bool isPlayer()
    {
        return true;
    }

    public override bool isEnemy()
    {
        return false;
    }

    // Use this for initialization
    protected override void Start()
    {
        animator = GetComponent<Animator>();
#if UNITY_ANDROID
        touchOrigin = -Vector2.one;
#endif
        food = GameManager.instance.playerFoodPoints;
        DisplayFood();
        base.Start();
    }

    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;
    }
	
    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.playersTurn)
        {
            return;
        }
        int horizontal = 0, vertical = 0;
#if UNITY_STANDALONE || UNITY_WEBGL
        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");
        if (horizontal != 0)
        {
            vertical = 0;
        }
#elif UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touchId = Input.touches[0];
            if (touchId.phase == TouchPhase.Began)
            {
                touchOrigin = touchId.position;
            }
            else if (touchId.phase == TouchPhase.Ended && touchOrigin.x >= 0)
            {
                Vector2 touchEnd = touchId.position;
                float x = touchEnd.x - touchOrigin.x;
                float y = touchEnd.y - touchOrigin.y;
                touchOrigin.x = -1;
                if (Mathf.Abs(x) > Mathf.Abs(y))
                {
                    horizontal = x > 0 ? 1 : -1;
                }
                else
                {
                    vertical = y > 0 ? 1 : -1;
                }
            }
        }
#endif
        if (horizontal != 0 || vertical != 0 || Input.GetKeyDown(KeyCode.Space))
        {
            PerformTurn(horizontal, vertical);
        }
    }

    public void LoseFood(int loss)
    {
        animator.SetTrigger("playerHit");
        food -= loss;
        foodText.text = "-" + loss / 10 + " Food: " + food / 10;
        CheckIfGameOver();
    }

    private void PerformTurn(int xDir, int yDir)
    {
        // This is common code betwen player and enemy, put it in the parent
        // class
        int x = position.x + xDir;
        int y = position.y + yDir;
        if ((xDir != 0 || yDir != 0)
            && x >= 0
            && x < GameManager.instance.boardScript.columns
            && y >= 0
            && y < GameManager.instance.boardScript.rows)
        {
            AttemptMove(x, y);
        }
        DisplayFood();
        CheckIfGameOver();
        GameManager.instance.playersTurn = false;
    }

    protected override void AttemptMove(int x, int y)
    {
        // do not modify tile, it is passed by value
        Tile tile = GameManager.instance.boardObjects[x, y];
        // do not modify occupant, it is passed by value
        BlockingObject occupant = tile.occupant;
        if (occupant != null)
        {
            if (occupant.isEnemy())
            {
                --food;
                Enemy hitEnemy = occupant as Enemy;
                animator.SetTrigger("playerChop");
                SoundManager.instance.RandomizeSoundEffects(attackSound1,
                    attackSound2);
                hitEnemy.LoseHealth(enemyDamage);
            }
            else if (occupant.isWall())
            {
                --food;
                Wall hitWall = occupant as Wall;
                hitWall.DamageWall(wallDamage);
                animator.SetTrigger("playerChop");
            }
        }
        else
        {
            --food;
            // common code, move to parent
            GameManager.instance.boardObjects[position.x, position.y].occupant
                = null;
            GameManager.instance.boardObjects[x, y].occupant = this;
            position.x = x;
            position.y = y;
            SoundManager.instance.RandomizeSoundEffects(moveSound1,
                moveSound2);
            Move(x, y);

            if (tile.exit)
            {
                Invoke("Restart", restartLevelDelay);
                enabled = false;
                GameManager.instance.levelOver = true;
            }
            else if (tile.food)
            {
                Food foodObject = tile.food;
                int pointsPerFood = foodObject.food;
                food += pointsPerFood;
                GainFood(pointsPerFood);
                if (foodObject.type == Food.Type.Food)
                {
                    SoundManager.instance.RandomizeSoundEffects(eatSound1,
                        eatSound2);
                }
                else if (foodObject.type == Food.Type.Drink)
                {
                    SoundManager.instance.RandomizeSoundEffects(drinkSound1,
                        drinkSound2);
                }
                foodObject.gameObject.SetActive(false);
            }
        }
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            SoundManager.instance.musicSource.Stop();
            SoundManager.instance.PlaySingle(gameOverSound);
            GameManager.instance.GameOver();
        }
    }

    private void DisplayFood()
    {
        foodText.text = "Food: " + food / 10;
    }

    private void GainFood(int pointsGained)
    {
        foodText.text = "+" + pointsGained / 10 + " Food: " + food / 10;
    }
}
