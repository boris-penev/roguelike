﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : BlockingObject
{
    public float moveTime = 0.1f;
    public LayerMask blockingLayer;
    private float inverseMoveTime;

    public override bool isWall()
    {
        return false;
    }

    // Use this for initialization
    protected virtual void Start()
    {
        inverseMoveTime = 1.0f / moveTime;
    }

    protected void Move(int x, int y)
    {
        StartCoroutine(SmoothMovement(new Vector2(x, y)));
    }

    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            transform.position = Vector3.MoveTowards(transform.position, end,
                    inverseMoveTime * Time.deltaTime);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    protected abstract void AttemptMove(int xDir, int yDir);
}
