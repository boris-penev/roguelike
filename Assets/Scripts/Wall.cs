﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : BlockingObject
{
    public AudioClip chopSound1, chopSound2;

    public override bool isWall()
    {
        return true;
    }

    public override bool isPlayer()
    {
        return false;
    }

    public override bool isEnemy()
    {
        return false;
    }

    public virtual void DamageWall(int loss)
    {
        SoundManager.instance.RandomizeSoundEffects(chopSound1, chopSound2);
    }
}
